//
//  ViewController.m
//  20160428_iOS_Day7_ObjectCClass
//
//  Created by ChenSean on 4/28/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "ViewController.h"
#import "MyObject.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    int a = 10;
    int b = 20;
    
    int ans1 = [MyObject add1:a withAnotherNumber:b];
    
    MyObject *myObj = [MyObject new];
    MyObject *myObj2 = [MyObject new];
    int ans2 = [myObj add1:a withAnotherNumber:b];
    
    // 只會有一份記憶體空間，static 的寫法
    MyObject *myStaticObj3 = [MyObject getMyObject];
    MyObject *myStaticObj4 = [MyObject getMyObject];
    myStaticObj3.i = 10;
    int result = myStaticObj4.i; // 會拿到 10
    
    
    // xcode 在用的方法就是用這個技巧
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
