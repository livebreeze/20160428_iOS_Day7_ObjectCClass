//
//  MyObject.h
//  20160428_iOS_Day7_ObjectCClass
//
//  Created by ChenSean on 4/28/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyObject : NSObject

@property int i;

+(instancetype) getMyObject;
- (int) add1: (int) a withAnotherNumber: (int) b;
+ (int) add1: (int) a withAnotherNumber: (int) b;

@end

static MyObject *_my_obj;