//
//  AppDelegate.h
//  20160428_iOS_Day7_ObjectCClass
//
//  Created by ChenSean on 4/28/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

