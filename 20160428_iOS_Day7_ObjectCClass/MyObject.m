//
//  MyObject.m
//  20160428_iOS_Day7_ObjectCClass
//
//  Created by ChenSean on 4/28/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "MyObject.h"

@implementation MyObject

+ (instancetype) getMyObject {
    if (_my_obj == nil) {
        _my_obj = [MyObject new];
    }
    
    return _my_obj;
}

// instance method
- (int) add1: (int) a withAnotherNumber: (int) b {
    return a + b;
}

// class method
+ (int) add1: (int) a withAnotherNumber: (int) b {
    return a + b;
}

@end
